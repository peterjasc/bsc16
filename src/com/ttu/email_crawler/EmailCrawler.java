package com.ttu.email_crawler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.net.InternetDomainName;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EmailCrawler implements Runnable {
    
  private static final int CRAWLER_TIMEOUT_IN_MILLISECONDS = 40000;

  private static final int MINUTE = 1000;

  private static final String WEBSITES_TO_CRAWL_FILE_NAME = "websitesToCrawl";

  private static final int AMOUNT_OF_LINKS_TO_ARTIFICIALLY_REMOVE_TO_AVOID_INFINITE_LOOP = 1;

  private static final String EMAILS_FILE_NAME = "emails";

  private static final int MILLISECONDS_TO_REST_BEFORE_CONT_CRAWL = 7000;

  private static final int HOW_MANY_LINKS_TO_CRAWL = 1000;

  private Thread t;

  private String threadName;

  private UrlValidator urlValidator;

  private String urlToCrawl;

  private final LinkedList<String> toBeCrawledHostUrls = new LinkedList<>();

  private static final Set<String> alreadyAddedUrls = new HashSet<>();

  private static final Set<String> alreadyAddedEmails = new HashSet<>();

    private EmailCrawler() {
    urlValidator = new UrlValidator();
  }

  private static Elements getUrlElementsInsideWebPage(String url)
      throws IOException, InterruptedException {
    try {
    Document doc = Jsoup.connect(url).timeout(CRAWLER_TIMEOUT_IN_MILLISECONDS).get();
        return doc.select("a[href]");
    } catch (UnknownHostException unknownHostException) {
      throw new UnknownHostException(unknownHostException.getMessage());
    } catch (HttpStatusException httpStatusException) {
      if (httpStatusException.getStatusCode() != 404 && httpStatusException.getStatusCode() !=408 && httpStatusException.getStatusCode() != 410){
      throw new HttpStatusException("this thread is done for...", httpStatusException.getStatusCode(), httpStatusException.getUrl());
      }
    } catch (SocketTimeoutException socketTimeoutException) {
      throw new SocketTimeoutException(socketTimeoutException.getMessage());
    } catch (SecurityException securityException) {
      throw new SecurityException(securityException.getMessage());
    } catch (Exception exception) {
      System.out.println(url);
      System.out.println("getUrlElementsInsideWebPage Exception");
      exception.getMessage();
    }
    return new Elements();
  }

  private LinkedList<String> addSubstringContainingDomainUrlsToLinkedList(String subString,
      Elements urls) throws IOException, InterruptedException {
    LinkedList<String> list = new LinkedList<>();
    for (Element link : urls) {
      String absoluteUrl = link.absUrl("href");
      if (absoluteUrl.isEmpty()) {
        continue;
      }

      if (urlValidator.isValid(absoluteUrl)
          && absoluteUrl.contains(subString)
          && !absoluteUrl.contains("jpg")
          && !absoluteUrl.contains("png")
          && !absoluteUrl.contains("gif")
          && !absoluteUrl.contains("pdf")) {
          list.add(absoluteUrl);
      }
    }
    sleepForSpecifiedRandomAmmountOfMilliseconds();
    return list;
  }

  private void sleepForSpecifiedRandomAmmountOfMilliseconds() throws InterruptedException {
    Thread.sleep(MILLISECONDS_TO_REST_BEFORE_CONT_CRAWL
        + ThreadLocalRandom.current().nextInt(-MINUTE, MINUTE));
  }

  LinkedList<String> addSubstringContainingUrlsToLinkedList(String subString,
        Elements urls) throws IOException, InterruptedException {
    LinkedList<String> list = new LinkedList<>();
    for (Element link : urls) {
      String absoluteUrl = link.absUrl("href");
      if (absoluteUrl.isEmpty()) {
        continue;
      }
      String cleanedUrl = cleanlUrl(absoluteUrl);
      if (urlValidator.isValid(cleanedUrl)
              && cleanedUrl.contains(subString)) {
        list.add(cleanedUrl);
      }
    }
    System.out.println("ListSize"+list.size());
    sleepForSpecifiedRandomAmmountOfMilliseconds();
    return list;
  }

  LinkedList<String> addSubstringLackingUrlTitlesToLinkedList(String subString,
                                                                      Elements urls) throws IOException, InterruptedException {
    LinkedList<String> list = new LinkedList<>();
    for (Element link : urls) {
      String url = link.attr("title");
      if (url.isEmpty()) {
        continue;
      }
      if (urlValidator.isValid(url)) {
        String cleanedUrl = cleanlUrl(url);
        if (!cleanedUrl.contains(subString)) {
          list.add(cleanedUrl);
        }
      }
    }
    System.out.println("List"+list.toString());
    saveIterableElementsToAFile(WEBSITES_TO_CRAWL_FILE_NAME, list);
    sleepForSpecifiedRandomAmmountOfMilliseconds();
    return list;
  }

  LinkedList<String> extractEmails(String toBeCrawledUrl) throws IOException {
    LinkedList<String> toBeAddedEmails = new LinkedList<>();
    try {
      Document doc = Jsoup.connect(toBeCrawledUrl).timeout(CRAWLER_TIMEOUT_IN_MILLISECONDS).get();
      Pattern p = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
      Matcher matcher = p.matcher(doc.text());
      while (matcher.find()) {
        String email = matcher.group();
        if (tryAddEmailToAlreadyAdded(email)) {
          toBeAddedEmails.add(email);
        }
      }
    } catch (Exception exception) {
      System.out.println(toBeCrawledUrl);
      System.out.println("extractEmails Exception");
      exception.printStackTrace();
      return toBeAddedEmails;
    }

    return toBeAddedEmails;
  }

  private String cleanlUrl(String uncleanUrl) throws IOException {
      URL url = new URL(uncleanUrl);
      return url.getProtocol() + "://" +
              url.getHost() + url.getPath();
  }

  private String getUrlsTopPrivateDomainFromAbsoluteUrl(String absoluteUrl) throws IOException {
      String host = new URL(absoluteUrl).getHost();
    return InternetDomainName.from(host).topPrivateDomain().name();
  }

  private static boolean tryAddUrlToAlreadyAdded(String url) {
      synchronized (alreadyAddedUrls) {
          if (alreadyAddedUrls.contains(url)) {
              return false;
          } else {
              alreadyAddedUrls.add(url);
              return true;
          }
      }
  }

  private static synchronized boolean tryAddEmailToAlreadyAdded(String email) {
      synchronized (alreadyAddedEmails) {
          if (alreadyAddedEmails.contains(email)) {
              return false;
          } else {
              alreadyAddedEmails.add(email);
              return true;
          }
      }
  }

    private int crawlForEmailsFromUrl(String url, int howManyLinks)
            throws IOException, InterruptedException {
        Elements urlElements = getUrlElementsInsideWebPage(url);
        LinkedList<String> domainsUrls =
                addSubstringContainingDomainUrlsToLinkedList(getUrlsTopPrivateDomainFromAbsoluteUrl(url), urlElements);
        LinkedList<String> emails = new LinkedList<>();

        if (domainsUrls.isEmpty()) {
            howManyLinks
                    = howManyLinks - AMOUNT_OF_LINKS_TO_ARTIFICIALLY_REMOVE_TO_AVOID_INFINITE_LOOP;
            return howManyLinks;
        }

        for (String domainsUrl : domainsUrls) {
            howManyLinks--;
            if (howManyLinks <= 0) {
                break;
            }

            if (!tryAddUrlToAlreadyAdded(domainsUrl)) {
                continue;
            }

            toBeCrawledHostUrls.add(domainsUrl);
            emails.addAll(extractEmails(domainsUrl));
        }

        Iterator<String> iterator = emails.iterator();
        while (iterator.hasNext()) {
            String email = iterator.next();
            if (!tryAddEmailToAlreadyAdded(email)) {
                iterator.remove();
            }
        }

        if (!emails.isEmpty()) {
            saveIterableElementsToAFile(EMAILS_FILE_NAME, emails);
        }
        System.out.println("emails"+emails.toString());

        return howManyLinks;
    }

  private void crawlNUrls(String startPointUrl)
          throws IOException, InterruptedException {
    int howManyLinks = HOW_MANY_LINKS_TO_CRAWL;
    String urlToCrawlForLinks = startPointUrl;
      while(howManyLinks >= 1) {
        howManyLinks = crawlForEmailsFromUrl(urlToCrawlForLinks, howManyLinks);
        if (toBeCrawledHostUrls.isEmpty()) {
          break;
        } else {
        urlToCrawlForLinks = toBeCrawledHostUrls.removeFirst();
        }
      }
  }

  private synchronized void saveIterableElementsToAFile(String fileName,
      Iterable<String> iterable) {
    BufferedWriter br = null;
    try {
      br = new BufferedWriter(new FileWriter(fileName + ".txt", true));
      for (String next : iterable) {
          br.write(next + "\n");
      }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
      try {
          if (br != null) {
              br.close();
          }
      } catch (IOException e) {
          e.printStackTrace();
      }
    }
  }

  EmailCrawler(String threadName, String url) throws IOException, InterruptedException {
    this();
    this.threadName = threadName;
    this.urlToCrawl = url;
  }
  
  public void run() {
    System.out.println("Running " +  threadName );
    try {
      crawlNUrls(urlToCrawl);
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }
  }
  
  void start () {
    if (t == null) {
       t = new Thread (this, threadName);
       t.start();
    }
  }
    
}

