package com.ttu.email_crawler;

import org.apache.commons.validator.routines.UrlValidator;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;


public class MultiThreadedEmailCrawler {
    
  public static void main(String args[]) throws IOException, InterruptedException {

//        EmailCrawler emailCrawler = new EmailCrawler();

//      String startingPointUrl = "http://www.freeindex.co.uk";
//      LinkedList<String> toBeUsedCategoryUrls = 
//          getCategoryUrls(emailCrawler, startingPointUrl);
    
//    LinkedList<String> toBeUsedProfileUrls = 
//          getProfileUrls(emailCrawler, getPreDefinedCategories());

    // if we are getting website urls from predefined ones
//    LinkedList<String> toBeCrawledWebsiteUrls = getPreDefinedProfiles();
//    LinkedList<String> websiteUrls = getWebsiteUrls(emailCrawler,toBeCrawledWebsiteUrls);

    LinkedList<String> websiteUrls = getPreDefinedWebsiteUrls();
    for (int i = 0; i < 60 && !websiteUrls.isEmpty(); i++) {
      try {
          new EmailCrawler("Thread " + i, websiteUrls.removeLast()).start();
      } catch (InterruptedException e) {
          System.out.println("Crawler" + i + "interrupted.");
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
    }

  }

//  private static LinkedList<String> getCategoryUrls(EmailCrawler emailCrawler,
//      String startingPointUrl) throws IOException, InterruptedException {
//    Elements urlElements = emailCrawler.getUrlElementsInsideWebPage(startingPointUrl);
//    LinkedList<String> toBeUsedCategoryUrls = 
//        emailCrawler.addSubstringContainingUrlsToLinkedList("/categories/", urlElements);
//    return toBeUsedCategoryUrls;
//  }
//

//  private static LinkedList<String> getProfileUrls(EmailCrawler emailCrawler,
//      LinkedList<String> toBeUsedCategoryUrls)
//          throws IOException, InterruptedException {
//    LinkedList<String> toBeUsedProfileUrls = new LinkedList<>();
//    for (String categoryUrl : toBeUsedCategoryUrls) {
//      Elements urlElements = emailCrawler.getUrlElementsInsideWebPage(categoryUrl);
//      toBeUsedProfileUrls
//      .addAll(emailCrawler
//          .addSubstringContainingUrlsToLinkedList("/profile", urlElements));
//    }
//    return toBeUsedProfileUrls;
//  }
  
//  private static LinkedList<String> getWebsiteUrls(EmailCrawler emailCrawler,
//  LinkedList<String> toBeUsedProfileUrls)
//      throws IOException, InterruptedException {
//    LinkedList<String> toBeCrawledWebsiteUrls = new LinkedList<>();
//    for (String profileUrl : toBeUsedProfileUrls) {
//    Elements urlElements = emailCrawler.getUrlElementsInsideWebPage(profileUrl);
//    toBeCrawledWebsiteUrls
//      .addAll(emailCrawler
//        .addSubstringLackingUrlTitlesToLinkedList("www.freeindex.co.uk", urlElements));
//    }
//    return toBeCrawledWebsiteUrls;
//  }

//  private static LinkedList<String> getPreDefinedCategories() {
//    return new LinkedList<>(Arrays.asList(
//        "http://www.freeindex.co.uk/categories/shopping/clothes/",
//        "http://www.freeindex.co.uk/categories/entertainment_and_lifestyle/healthcare/complementary_therapy/",
//        "http://www.freeindex.co.uk/categories/computers_and_internet/computer_services/computer_repair/",
//        "http://www.freeindex.co.uk/categories/advertising_and_marketing/corporate_events/conferences/",
//        "http://www.freeindex.co.uk/categories/travel_and_tourism/accommodation/cottages/",
//        "http://www.freeindex.co.uk/categories/transport_and_auto/logistics/couriers/",
//        "http://www.freeindex.co.uk/categories/property/construction/painters_and_decorators/",
//        "http://www.freeindex.co.uk/categories/transport_and_auto/driving_schools/driving_instructors/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/education/",
//        "http://www.freeindex.co.uk/categories/property/construction/electricians/",
//        "http://www.freeindex.co.uk/categories/property/estate_agents/",
//        "http://www.freeindex.co.uk/categories/industry/agriculture/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/sports/fitness/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/shopping/florists/",
//        "http://www.freeindex.co.uk/categories/property/construction/landscape_gardeners/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/gifts/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/beauty/hairdressers/",
//        "http://www.freeindex.co.uk/categories/travel_and_tourism/accommodation/hotels/",
//        "http://www.freeindex.co.uk/categories/financial_and_accounting/insurance/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/shopping/jewellery/",
//        "http://www.freeindex.co.uk/categories/financial_and_legal/legal/lawyers/",
//        "http://www.freeindex.co.uk/categories/property/security/locksmiths/",
//        "http://www.freeindex.co.uk/categories/industry/manufacturing/",
//        "http://www.freeindex.co.uk/categories/advertising_and_marketing/advertising_and_marketing_consulting/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/music/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/parties_and_events/",
//        "http://www.freeindex.co.uk/categories/financial_and_accounting/consumer_finance/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/photography/photographers/",
//        "http://www.freeindex.co.uk/categories/property/construction/plastering/",
//        "http://www.freeindex.co.uk/categories/property/construction/plumbing_services/",
//        "http://www.freeindex.co.uk/categories/advertising_and_marketing/advertising_production_services/printing/",
//        "http://www.freeindex.co.uk/categories/human_resources/recruitment_agency/",
//        "http://www.freeindex.co.uk/categories/property/removals_and_relocation/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/restaurants/",
//        "http://www.freeindex.co.uk/categories/financial_and_legal/legal/solicitors/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/sports/",
//        "http://www.freeindex.co.uk/categories/property/consulting_services/land_surveyors/",
//        "http://www.freeindex.co.uk/categories/travel_and_tourism/taxi_services/",
//        "http://www.freeindex.co.uk/categories/travel_and_tourism/travel_agents/",
//        "http://www.freeindex.co.uk/categories/transport_and_auto/vehicle_hire/van_hire/",
//        "http://www.freeindex.co.uk/categories/computers_and_internet/web_design_resources/web_designers/",
//        "http://www.freeindex.co.uk/categories/computers_and_internet/web_development/",
//        "http://www.freeindex.co.uk/categories/arts_and_lifestyle/parties_and_events/weddings/"
//    ));
//  }
  
//  private static LinkedList<String> getPreDefinedProfiles() {
//    return new LinkedList<>(Arrays.asList(
//        "http://www.freeindex.co.uk/profile(the-striped-box-food-company-ltd)_308539.htm",
//        "http://www.freeindex.co.uk/profile(ready2eat-african-meals-ltd)_669764.htm",
//        "http://www.freeindex.co.uk/profile(red-hot-meals)_563572.htm",
//        "http://www.freeindex.co.uk/profile(lancashire-fayre)_529578.htm",
//        "http://www.freeindex.co.uk/profile(blueberry-hill-meals)_508809.htm",
//        "http://www.freeindex.co.uk/profile(lucy-s-kitchen)_321275.htm",
//        "http://www.freeindex.co.uk/profile(trishul-tiffin-services)_307733.htm",
//        "http://www.freeindex.co.uk/profile(thirst-first-mobile-cocktail-bar-hire)_509971.htm",
//        "http://www.freeindex.co.uk/profile(book-a-bar-drunken-hog-catering)_343214.htm",
//        "http://www.freeindex.co.uk/profile(mojito-events)_115547.htm",
//        "http://www.freeindex.co.uk/profile(the-bespoke-bartender-company)_157829.htm",
//        "http://www.freeindex.co.uk/profile(the-old-exchange-holbeach)_576611.htm",
//        "http://www.freeindex.co.uk/profile(bars-2-you)_217820.htm",
//        "http://www.freeindex.co.uk/profile(crimson-mobile-bars)_492701.htm",
//        "http://www.freeindex.co.uk/profile(indispense)_51115.htm"
//      ));
//  }
  
  private static LinkedList<String> getPreDefinedWebsiteUrls() {
    return new LinkedList<>(Arrays.asList(
            "http://premierlimosltd.com/",
            "http://www.topmarquelimos.co.uk",
            "http://www.homejameslimousines.co.uk",
            "http://www.limo-king.co.uk",
            "http://www.earlslimousines.co.uk",
            "http://www.phantomlimo.co.uk",
            "http://www.lurganlimos.com",
            "http://www.swansealimos.co.uk",
            "http://www.jo-lolimousines.co.uk",
            "http://www.choicelimousines.co.uk",
            "http://www.starsandstripeslimousines.co.uk",
            "http://www.limosnorthwest.co.uk",
            "http://www.allstretchedout.co.uk",
            "http://www.xtreme-limos.co.uk",
            "http://www.homejameslimos.com",
            "http://www.fresh-limousines.com",
            "http://711travel.co.uk",
            "http://www.nbtminibus.co.uk",
            "http://www.avonminibuses.com",
            "http://www.milburnluxurycoaches.co.uk",
            "http://www.mini-bushire.co.uk",
            "http://www.roysminibuses.co.uk",
            "http://www.xtraminicoachhire.com",
            "http://www.aceexecutivetravel.co.uk",
            "http://www.justbusleeds.co.uk",
            "http://www.swiftminibushire.co.uk",
            "http://www.warringtonminibushire.com",
            "http://www.birleymoortravel.com",
            "http://www.aj-travel.co.uk",
            "http://www.minibushiremiddlesbrough.net",
            "http://www.2amtravel.co.uk",
            "http://www.go-goscotland.com",
            "http://www.stminibushire.co.uk/",
            "http://www.tjstravel.co.uk",
            "http://www.a2bvanhire.co.uk",
            "http://www.wheelchairtaxisthelens.co.uk",
            "http://www.yourminibus.co.uk",
            "http://www.gateway2lease.com",
            "http://www.carlease.uk.com",
            "http://www.danielmyersltd.com",
            "http://www.vantage-leasing.com",
            "http://www.alternativeroutefinance.co.uk",
            "http://www.flexeonvehiclescontracts.co.uk",
            "http://www.leaseline-scotland.co.uk",
            "http://www.octopus-leasing.co.uk",
            "http://www.dsgauto.com/",
            "http://www.printempire.co.uk",
            "http://www.fuelcardservices.com/",
            "http://www.genusleasing.com/",
            "http://www.seaswineautomotive.com",
            "http://www.planyourcar.com",
            "http://www.gibson.multileasingdirect.co.uk",
            "http://www.anthonyk.co.uk/business-lease-cars",
            "http://www.gkluk.com/",
            "http://www.astutemotoring.com",
            "http://www.leasecarsonline.co.uk",
            "http://www.prestigeleaseyorkshire.co.uk",
            "http://www.commercialvehiclecontracts.co.uk",
            "http://www.moveleasing.co.uk",
            "http://www.henleyclassiccarhire.co.uk",
            "http://www.spiritweddingservices.co.uk",
            "http://www.cloudnineclassicweddings.co.uk",
            "http://www.gspweddingcars.co.uk/",
            "http://cavalcades.co.uk",
            "http://www.silverstarweddingcars.co.uk",
            "http://www.redroseweddingcarhire.com",
            "http://www.sheergoldweddingcars.co.uk",
            "http://www.americandreams.co.uk",
            "http://www.londonlegend.co.uk",
            "http://vintagesportscarhire.com",
            "http://www.barringtonslimousines.co.uk",
            "http://www.bigdaybuses.com",
            "http://www.prestigecarsgrangemouth.co.uk",
            "http://www.dreamweddingcars.co.uk",
            "http://www.weddingcarshalifax.com",
            "http://www.simplystunningcars.co.uk",
            "http://www.luxurydrive.co.uk",
            "http://www.englandsfinestcars.co.uk",
            "http://www.specialdaycars.com",
            "http://www.nottingham-chauffeurs.com",
            "http://www.reeceweddingcars.com",
            "http://www.phoenix-limousines-plymouth.co.uk/",
            "http://www.alfredsweddingcar.co.uk",
            "http://www.carsforstars.co.uk",
            "http://www.4x4vehiclehire.co.uk/4x4-hire/birmingham/",
            "http://www.flaxtonselfdrive.co.uk",
            "http://indigocarhire.co.uk/",
            "http://www.cars4rental.co.uk",
            "http://www.danickleasing.co.uk",
            "http://www.premierevelocity.com",
            "http://www.avis.co.uk",
            "http://www.capitalcarandvanrental.com/",
            "http://www.carhiremarket.com",
            "http://www.courtesyvehiclesolutions.com",
            "http://www.west1autogroup.com",
            "http://www.execcarhire.co.uk",
            "http://www.delightfuldining.co.uk",
            "http://www.maroonspice.com",
            "http://www.sandrastarlingcatering.co.uk"
            ));
  }
}
