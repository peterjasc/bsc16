package com.ttu.email_search;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

import com.ttu.file_processing.FileHandler;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import static com.ttu.email_search.GoogleCustomSearch.HOW_MANY_PAGES_FOR_ONE_QUERY;
import static com.ttu.email_statistics.PageStatistics.TXT;
import static com.ttu.file_processing.FileHandler.saveIterableElementsToAFilePath;


public class BingSearch {

    private static final String QUERY_TYPE = "emails";
    private static final String FILE_PATH_WHERE_TO_GET_EMAILS = "bing_bs_" + QUERY_TYPE + ".txt";
    private static final String DIRECTORY_PATH_WHERE_TO_SAVE_LINKS =
        "bing_" + QUERY_TYPE + "_exact";

    public static void main(final String[] args) throws Exception {
        int querysExecuted = 0;
        ArrayList<String> querys = FileHandler.getLinesFromAFile(FILE_PATH_WHERE_TO_GET_EMAILS);

        for (String query : querys) {
            System.out.println("Q:" + query);
//            int pageNumberForQuery = 0;

            if (querysExecuted % 100 == 0 && querysExecuted != 0) {
                System.out.println("------------------------------------------");
                System.out.println(querysExecuted);
                System.out.println("------------------------------------------");
                Thread.sleep(10000);
            }

            getBingResults(query);
            querysExecuted++;
//            pageNumberForQuery++;
//            if (pageNumberForQuery > HOW_MANY_PAGES_FOR_ONE_QUERY ) {
//                break;
//            }
        }
    }

    private static void getBingResults(final String query) throws Exception {

        final LinkedList<String> urlListForEmail = new LinkedList<>();
        int resultsLength;
        String bingUrlPattern = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?"
            + "Query=%%27%s%%27"
            + "&$format=JSON"
            + "&WebSearchOptions=%%27DisableQueryAlterations%%27";

        final String accountKey = "qyhG3m5hmJy1aAmnetUrytuve2/rHXlH8izpyoK3xGs";
//        if (skipNumber == 1){
//        } else {
//            //$skip=100 -- second page
//            bingUrlPattern
//                    = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?Query=%%27%s%%27&$format=JSON&$skip="
//                    + (skipNumber - 1) * 100;
//        }
        String exactQuery = GoogleCustomSearch.getExactlyEmailAndOrFullName(query);
        final String encodedQuery = URLEncoder.encode(exactQuery, "UTF-8");
        final String bingUrl = String.format(bingUrlPattern, encodedQuery);
//        System.out.println("bingURL" + bingUrl);

        // API 1.8 Problem was:
        // final String accountKeyEnc = Base64.getEncoder().encodeToString((accountKey + ":" + accountKey).getBytes());
        final String accountKeyForBase64Encoding = accountKey + ":" + accountKey;
        final String accountKeyEnc = new String (Base64.encodeBase64(accountKeyForBase64Encoding.getBytes()));
        final URL url = new URL(bingUrl);
        final URLConnection connection = url.openConnection();
        connection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);

        try (final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            final StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            final JSONObject json = new JSONObject(response.toString());
            final JSONObject d = json.getJSONObject("d");
            final JSONArray results = d.getJSONArray("results");
            resultsLength = results.length();
            System.out.println(resultsLength);
            for (int i = 0; i < resultsLength; i++) {
                final JSONObject aResult = results.getJSONObject(i);
//                System.out.println("URL:" + aResult.get("Url"));
                urlListForEmail.add((String)aResult.get("Url"));
            }
        }
        if (urlListForEmail.size() != 0) {
            saveIterableElementsToAFilePath(urlListForEmail,
                DIRECTORY_PATH_WHERE_TO_SAVE_LINKS + File.separator + query + TXT);
        }
//        if (resultsLength == 50) {
//            skipNumber++;
//        } else {
//            skipNumber = 0;
//        }

//        return skipNumber;
    }

}
