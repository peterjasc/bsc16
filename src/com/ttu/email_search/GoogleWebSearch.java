package com.ttu.email_search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.LinkedList;

import com.google.gson.Gson;

/* licensed under cc by-sa 3.0 with attribution required
 * https://creativecommons.org/licenses/by-sa/3.0/
 * original author BalusC
 */
public class GoogleWebSearch {

    private static final int HOW_MANY_PAGES_FOR_ONE_QUERY = 5;
  
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
//        LinkedList<String> emails = FileHandler.getLinesFromAFile("bs_emails");
//        getNNumberPagesForEmails(emails);
        GoogleResults googleResults = getSearchQueryResult("jcgregorio@google.com");
        printToConsoleInReadableFormat(googleResults);


    }

    private static void getNNumberPagesForEmails(LinkedList<String> emails) throws InterruptedException, IOException, URISyntaxException {
        int querysExecuted = 0;
//        GoogleResults results = getSearchQueryResult(queryValue);
//        printToConsoleInReadableFormat(results);

        for (String email : emails) {
            int pageReqLimit = 0;
            Long indexOfFirstResultToReturn = -1L;
            while (indexOfFirstResultToReturn != 0L) {
                GoogleResults queryResult = getSearchQueryResult(email);

                System.out.println("indexOfFirstResultToReturn"+indexOfFirstResultToReturn);
                System.out.println("---");

                querysExecuted++;
                if (querysExecuted % 100 == 0) {
                    Thread.sleep(100000);
                }

                if (pageReqLimit <= HOW_MANY_PAGES_FOR_ONE_QUERY - 1) {
                    pageReqLimit++;
                } else {
                    break;
                }
            }
        }
    }

    private static void saveSearchQueryResult(LinkedList<String> emails,
                                                      Long indexOfFirstResultToReturn)
            throws URISyntaxException, IOException {

    }

    private static GoogleResults getSearchQueryResult(String queryValue) throws URISyntaxException, IOException {
        URL url = new URL(
           "https://ajax.googleapis.com/ajax/services/search/web?v=1.0&"
           + "q=" + URLEncoder.encode(queryValue, "UTF-8")
           + "&userip=" + URLEncoder.encode(InetAddress.getLocalHost().getHostAddress(), "UTF-8"));
        System.out.println(url.toURI());
        URLConnection connection = url.openConnection();

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        return new Gson().fromJson(reader, GoogleResults.class);
    }

    private static void printToConsoleInReadableFormat(GoogleResults results) {
        int total = results.getResponseData().getResults().size();
        System.out.println("total: "+total);
        for(int i=0; i<=total-1; i++){
            System.out.println("Title: " + results.getResponseData().getResults().get(i).getTitle());
            System.out.println("URL: " + results.getResponseData().getResults().get(i).getUrl() + "\n");

        }
    }
}
