package com.ttu.email_search;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import com.ttu.email_statistics.PageStatistics;
import com.ttu.file_processing.FileHandler;

import java.io.File;
import java.util.*;

import static com.ttu.email_statistics.PageStatistics.QUERY_SEPARATOR;

public class GoogleCustomSearch {

    static final int HOW_MANY_PAGES_FOR_ONE_QUERY = 5;
    private static final String QUERY_TYPE = "fullNames";
    private static final String FILE_PATH_WHERE_TO_GET_EMAILS = "bs_" + QUERY_TYPE + ".txt";
    private static final String DIRECTORY_PATH_WHERE_TO_SAVE_LINKS = QUERY_TYPE + "_exact";
    private static final String DOUBLE_QUOTE_MARK_IN_STRING = "\"";
    public static final String PLUS_SIGN = "+";

    public static void main(String[] args) throws InterruptedException {

        GoogleCustomSearch gsc = new GoogleCustomSearch();
        ArrayList<String> emails = FileHandler.getLinesFromAFile(FILE_PATH_WHERE_TO_GET_EMAILS);

        getNNumberPagesForEmails(gsc, emails);
    }

    private static void getNNumberPagesForEmails(GoogleCustomSearch gsc, ArrayList<String> emails) throws InterruptedException {
        int querysExecuted = 0;

        for (String email : emails) {
            int pageNumberForQuery = 0;
            Long indexOfFirstResultToReturn = -1L;
            while (indexOfFirstResultToReturn != 0L) {

                if (querysExecuted % 100 == 0 && querysExecuted != 0) {
                    System.out.println("------------------------------------------");
                    System.out.println(querysExecuted);
                    System.out.println("------------------------------------------");
                    Thread.sleep(100000);
                }

                pageNumberForQuery++;
                if (pageNumberForQuery > HOW_MANY_PAGES_FOR_ONE_QUERY ) {
                    break;
                }

                indexOfFirstResultToReturn = gsc.saveSearchQueryResult(email, indexOfFirstResultToReturn);
                System.out.println("indexOfFirstResultToReturn"+indexOfFirstResultToReturn);
                querysExecuted++;
            }
            System.out.println("---");
        }
    }

    private Long saveSearchQueryResult(String searchKeyWords, Long indexOfFirstResultToReturn) {

        final String API_KEY = "AIzaSyAP4iTZGRxgTYXrmzH1hDChs3WgJbBgYNY";
        final String SEARCH_ENGINE_ID = "006979156886297148192:xb608ncxksk";
        final Customsearch customsearch;

        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        Customsearch.Builder customBuilder = new Customsearch.Builder(httpTransport, jsonFactory, null);
        customBuilder.setApplicationName("GCSapp");

        customsearch = customBuilder.build();
        List<Result> resultList;


        String exactQuery = getExactlyEmailAndOrFullName(searchKeyWords);
        System.out.println("EXACT_QUERY:" + exactQuery);
        try {
            Customsearch.Cse.List list = customsearch.cse().list(exactQuery);
            list.setKey(API_KEY);
            list.setCx(SEARCH_ENGINE_ID);

            if (indexOfFirstResultToReturn != -1L) {
                list.setStart(indexOfFirstResultToReturn);
            } else {
                indexOfFirstResultToReturn = 1L;
            }

            Search results = list.execute();
            resultList = results.getItems();

            if (resultList != null) {
                saveResultListAsKeywordNamedFile(searchKeyWords, resultList);
            }


            System.out.println(results.getQueries().get("request"));
            System.out.println(results.getQueries().get("nextPage"));

            if (results.getQueries().get("nextPage") != null) {
                return indexOfFirstResultToReturn
                        + Long.valueOf(results.getQueries().get("nextPage").get(0).getCount());
            } else {
                return 0L;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        return 0L;
    }

    static String getExactlyEmailAndOrFullName(String searchKeyWords) {
        String[] splitSearchKeywords = searchKeyWords.split(QUERY_SEPARATOR,3);
        String exactQuery = "";
        if (splitSearchKeywords.length == 3) {
            exactQuery = PLUS_SIGN + DOUBLE_QUOTE_MARK_IN_STRING
                + splitSearchKeywords[0]
                + DOUBLE_QUOTE_MARK_IN_STRING
                + QUERY_SEPARATOR
                + PLUS_SIGN + DOUBLE_QUOTE_MARK_IN_STRING
                + splitSearchKeywords [1] + QUERY_SEPARATOR + splitSearchKeywords[2]
                + DOUBLE_QUOTE_MARK_IN_STRING;

        } else if (splitSearchKeywords.length == 2) {
            exactQuery = PLUS_SIGN + DOUBLE_QUOTE_MARK_IN_STRING
                + splitSearchKeywords[0]
                + QUERY_SEPARATOR
                + splitSearchKeywords[1]
                + DOUBLE_QUOTE_MARK_IN_STRING;
        } else if (splitSearchKeywords.length == 1) {
            exactQuery = PLUS_SIGN + DOUBLE_QUOTE_MARK_IN_STRING
                + splitSearchKeywords[0]
                + DOUBLE_QUOTE_MARK_IN_STRING;
        }

        return exactQuery;
    }

    private static boolean saveResultListAsKeywordNamedFile(String searchKeyWord, List<Result> resultsFromSearch) {

        final Set<String> resultsSavedToFile  = new LinkedHashSet<>();
            for (Result resultFromSearch : resultsFromSearch) {
                resultsSavedToFile.add(resultFromSearch.getLink());
            }
        if (!resultsFromSearch.isEmpty()) {
            FileHandler.saveIterableElementsToAFilePath(
                resultsSavedToFile,
                DIRECTORY_PATH_WHERE_TO_SAVE_LINKS + File.separator + searchKeyWord + ".txt");
            return true;
        } else {
            return false;
        }
    }

}
