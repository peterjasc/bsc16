package com.ttu.email_statistics;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static com.ttu.email_statistics.PageStatistics.QUERY_SEPARATOR;
import static com.ttu.email_statistics.PageStatistics.SEPARATOR;
import static com.ttu.email_statistics.PageStatistics.getNthWordInLowerCase;

class CacheAccessor {

    static int addPresenceIntoCache(String key, LinkedHashMap<String, Integer> presenceCounter) {
        int numberOfTimes;
        if (presenceCounter.get(key) != null) {
            numberOfTimes = presenceCounter.get(key);
            presenceCounter.put(key, ++numberOfTimes);
        } else {
            System.out.println("addPresenceIntoCache() presenceCounter doesnt have " + key);
            throw new NullPointerException("addPresenceIntoCache() presenceCounter doesnt have " + key);
        }
        return numberOfTimes;
    }

    static void putLineIntoCache(ArrayList<String> namesLines,
        HashMap<String, Integer> handledList) {
        for (String nameLine : namesLines) {
            String[] splitNameLine = nameLine.split(SEPARATOR);
            String name = getNthWordInLowerCase(0, splitNameLine);
            Integer numberOfOccurences = Integer.valueOf(splitNameLine[1]);
            handledList.put(name,numberOfOccurences);
        }
    }

    static boolean isPresentInLinesOfWebsite(ArrayList<String> listOfLines, String keyword) {
        for (int wordLocation = 0; wordLocation < listOfLines.size(); wordLocation++) {
            if(StringUtils.containsIgnoreCase(listOfLines.get(wordLocation), keyword)) {
                return true;
            } else if (checkNeighbouringWordOnLeft(listOfLines, keyword, wordLocation)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkNeighbouringWordOnLeft(ArrayList<String> listOfLines,
        String keyword, int wordLocation) {
        if (canLookLeft(wordLocation)) {
            if (lookOneLeft(listOfLines, keyword, wordLocation)) {
                return true;
            }
        }

        return false;
    }

    private static boolean lookOneLeft(ArrayList<String> listOfLines, String keyword,
        int wordLocation) {
        return StringUtils
            .containsIgnoreCase(listOfLines.get(wordLocation - 1)
                + QUERY_SEPARATOR + listOfLines.get(wordLocation),keyword);
    }

    private static boolean canLookLeft (int wordLocationInHtml) {
        return wordLocationInHtml - 1 >= 0;
    }

}
