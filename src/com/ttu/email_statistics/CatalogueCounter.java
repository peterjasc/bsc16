package com.ttu.email_statistics;

import com.ttu.email_statistics.PageStatistics.WhichGender;
import com.ttu.file_processing.FileHandler;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import static com.ttu.email_statistics.PageStatistics.SEPARATOR;
import static com.ttu.email_statistics.PageStatistics.TEXT_ANALYSIS_DIR;
import static com.ttu.email_statistics.CacheAccessor.addPresenceIntoCache;
import static com.ttu.email_statistics.CacheAccessor.putLineIntoCache;
import static com.ttu.file_processing.BabyNamesDirHandler.BOY_NAMES_TXT;
import static com.ttu.file_processing.BabyNamesDirHandler.GIRL_NAMES_TXT;

class CatalogueCounter {

    private LinkedHashMap<String, Integer> namePresence = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> nameInLinkPresence = new LinkedHashMap<>();
    private static HashMap<String,Integer> handledBoyBabyNames = new HashMap<>();
    private static HashMap<String,Integer> handledGirlBabyNames = new HashMap<>();

    LinkedHashMap<String, Integer> getNameInLinkPresence() {
        return nameInLinkPresence;
    }

    LinkedHashMap<String, Integer> getNamePresence() {
                                                   return namePresence;
                                                                       }

    void addCatalogueCounterEntry(String link) {
        for (WhichGender whichGender : WhichGender.values()) {
            namePresence.put(link + SEPARATOR + whichGender.toString(), 0);
            nameInLinkPresence.put(link + SEPARATOR + whichGender.toString(), 0);
        }
   }

    void addNamePresenceOccurrencesInWebsitesText(String websitesText, String linkInFile) {
        for (String word : websitesText.split(" ")) {
            word = word.toLowerCase();
            countPresenceOfGenderAssignedNames(linkInFile, word, namePresence);
        }
    }

    void addNamePresenceOccurrencesInWebsitesLinks(Elements websiteLinks, String linkInFile) {
        for (Element websiteLink : websiteLinks) {
            String linkText = websiteLink.text().toLowerCase();
            for (String word : linkText.split(" ")) {
                countPresenceOfGenderAssignedNames(linkInFile, word, nameInLinkPresence);
            }
        }
    }

    private void countPresenceOfGenderAssignedNames(String linkInFile, String word, LinkedHashMap<String, Integer> names) {
        if (handledGirlBabyNames.containsKey(word) && handledBoyBabyNames.containsKey(word)) {
            addPresenceIntoCache(linkInFile + SEPARATOR + WhichGender.BOTH.toString(), names);
        } else if (handledGirlBabyNames.containsKey(word)) {
            addPresenceIntoCache(linkInFile + SEPARATOR + WhichGender.GIRL.toString(), names);
        } else if (handledBoyBabyNames.containsKey(word)) {
            addPresenceIntoCache(linkInFile + SEPARATOR + WhichGender.BOY.toString(), names);
        }
    }

    static void fillBothBabyNamesCaches() {
        ArrayList<String> boysNamesLines =
            FileHandler.getLinesFromAFile(TEXT_ANALYSIS_DIR + File.separator + BOY_NAMES_TXT);
        ArrayList<String> girlNamesLines =
            FileHandler.getLinesFromAFile(TEXT_ANALYSIS_DIR + File.separator + GIRL_NAMES_TXT);
        putLineIntoCache(girlNamesLines, handledGirlBabyNames);
        putLineIntoCache(boysNamesLines, handledBoyBabyNames);
    }

}
