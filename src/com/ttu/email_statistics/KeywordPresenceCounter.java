package com.ttu.email_statistics;

import com.ttu.email_statistics.PageStatistics.WhichSubstring;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.ttu.email_statistics.CacheAccessor.addPresenceIntoCache;
import static com.ttu.email_statistics.CacheAccessor.isPresentInLinesOfWebsite;
import static com.ttu.email_statistics.PageStatistics.*;

class KeywordPresenceCounter {

    private LinkedHashMap<String, Integer> keywordPresenceInText = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> keywordPresenceInLinks = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> filePresenceForQuerys = new LinkedHashMap<>();

    void countKeywordPresence(ArrayList<String> textFromFile,
        ArrayList<String> linksFromFile, String emailAndFullName)
        throws InterruptedException {

        addKeywordsPresence(textFromFile, emailAndFullName, keywordPresenceInText);
        addKeywordsPresence(linksFromFile, emailAndFullName, keywordPresenceInLinks);
    }

    void countFilePresenceForQuery(String query) {
        CacheAccessor.addPresenceIntoCache(query, filePresenceForQuerys);
        CacheAccessor.addPresenceIntoCache(getQueryEmailKey(query), filePresenceForQuerys);
        CacheAccessor.addPresenceIntoCache(getQueryFullNameKey(query), filePresenceForQuerys);
    }

    private static String getQueryFromEmailAndFullName(String emailAndFullName) {
        String query = emailAndFullName;
        if (NUMBER_OF_KEYWORDS == 1 && WHICH_SUBSTRING.equals(WhichSubstring.EMAIL)) {
            query = emailAndFullName.split(SEPARATOR,2)[0];
        } else if (NUMBER_OF_KEYWORDS == 1 && WHICH_SUBSTRING.equals(WhichSubstring.FULL_NAME)) {
            query = emailAndFullName.split(SEPARATOR,2)[1];
        }
        return query;
    }

    private static void addKeywordsPresence(ArrayList<String> linesInWebsite,
        String emailAndFullName, LinkedHashMap<String, Integer> presenceCounter) {

        String email = emailAndFullName.split(QUERY_SEPARATOR,2)[0];
        String fullName = emailAndFullName.split(QUERY_SEPARATOR,2)[1];
        String query = getQueryFromEmailAndFullName(emailAndFullName);
        boolean emailPresentInLinesOfWebsite =
            isPresentInLinesOfWebsite(linesInWebsite, email);
        boolean fullNamePresentInLinesOfWebsite =
            isPresentInLinesOfWebsite(linesInWebsite, fullName);

        if(emailPresentInLinesOfWebsite && fullNamePresentInLinesOfWebsite){
            addPresenceIntoCache(query, presenceCounter);
        }

        if(emailPresentInLinesOfWebsite){
            addPresenceIntoCache(getQueryEmailKey(query), presenceCounter);
        }

        if(fullNamePresentInLinesOfWebsite){
            addPresenceIntoCache(getQueryFullNameKey(query), presenceCounter);
        }
    }

    void addQueryPresenceEntry(String query) {
        putZerosIntoPresenceCounter(keywordPresenceInText, query);
        putZerosIntoPresenceCounter(keywordPresenceInLinks, query);
        putZerosIntoPresenceCounter(filePresenceForQuerys, query);
    }

    private static void putZerosIntoPresenceCounter(LinkedHashMap<String, Integer> presenceCounter, String query) {
        presenceCounter.put(query, 0);
        presenceCounter.put(getQueryEmailKey(query), 0);
        presenceCounter.put(getQueryFullNameKey(query), 0);
    }

    private static String getQueryEmailKey(String query) {
        return query + SEPARATOR
            + WhichSubstring.EMAIL.toString();
    }

    private static String getQueryFullNameKey(String query) {
        return query + SEPARATOR + WhichSubstring.FULL_NAME.toString();
    }

    LinkedHashMap<String, Integer> getKeywordPresenceInText() {
        return keywordPresenceInText;
    }

    LinkedHashMap<String, Integer> getKeywordPresenceInLinks() {
        return keywordPresenceInLinks;
    }

    LinkedHashMap<String, Integer> getFilePresenceForQuerys() {
        return filePresenceForQuerys;
    }
}
