package com.ttu.email_statistics;

import com.google.common.net.InternetDomainName;
import com.ttu.file_processing.FileHandler;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import static com.ttu.email_statistics.PageStatistics.SEPARATOR;
import static com.ttu.email_statistics.PageStatistics.TEXT_ANALYSIS_DIR;

class WebCrawler implements Runnable {

    private static final boolean APPEND_HTML_TO_TEXT_FILE = false;
    private Thread t;
    private String threadName;
    private static final LinkedList<String> filesFromDir = new LinkedList<>();

    private static final AtomicInteger CRAWLER_SLEEP_IN_MILLISECONDS = new AtomicInteger(30000);
    private static final HashSet<String> alreadyCrawledDomain = new HashSet<>();
    private static final HashSet<String> domainsThatForbidCrawling = new HashSet<>();
    private static final String AlphaNumericWhitespace = "[^A-Za-z0-9\\._\\-\\s@]";

    private static final String EXCEPTIONS_DIR = "Exceptions";
    private static String DIRECTORY_WHERE_TO_GET_LINKS = "emailsAndFullNames_exact";
    private static String CRAWLED_LINKS_TEXT_DIR = "crawledLinksText";
    private static String CRAWLED_LINKS_LINK_ELEMENTS_DIR = "crawledLinksLinkElements";
    private static final AtomicInteger MAX_SIZE_OF_HTML_FILE = new AtomicInteger(600000);

    private static String
        CAUGHT_EXCEPTIONS_FILE_PATH = EXCEPTIONS_DIR + File.separator
        + DIRECTORY_WHERE_TO_GET_LINKS + "caughtExceptions.txt";

    private WebCrawler(String threadName) throws IOException, InterruptedException {
        this.threadName = threadName;
    }

    private void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start();
        }
    }

    public void run() {
        System.out.println("Running " +  threadName );
        String fileName;
        synchronized (filesFromDir) {
            fileName = getFileNameInDir();
        }

        while (!fileName.equals("")) {
            System.out.println("Q:" + fileName);
            getHtmlFromLinksInFile(fileName);
            synchronized (filesFromDir) {
                fileName = getFileNameInDir();
            }
        }
    }

    public static void main(String args[]) throws IOException {

        if (args.length == 3) {
            DIRECTORY_WHERE_TO_GET_LINKS = args[0];
            CRAWLED_LINKS_TEXT_DIR = args[1];
            CRAWLED_LINKS_LINK_ELEMENTS_DIR = args[2];
            CAUGHT_EXCEPTIONS_FILE_PATH = TEXT_ANALYSIS_DIR + File.separator
                + DIRECTORY_WHERE_TO_GET_LINKS + "caughtExceptions.txt";
        }
        synchronized (filesFromDir) {
            WebCrawler.filesFromDir
                .addAll(FileHandler
                    .getFullFileNamesFromFilePathInAlphabeticalOrder(DIRECTORY_WHERE_TO_GET_LINKS));
        }

        for (int i = 0; i < 600; i++) {
                try {
                    new WebCrawler("Thread " + i).start();
                } catch (InterruptedException e) {
                    System.out.println("Crawler" + i + "interrupted.");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.out.println("Crawler" + i + "IO.");
                    e.printStackTrace();
                }
        }

        boolean endOfCrawling = false;
        while(!endOfCrawling) {
            try {
                Thread.sleep(CRAWLER_SLEEP_IN_MILLISECONDS.get());
                synchronized (alreadyCrawledDomain) {
                        if (WebCrawler.alreadyCrawledDomain.isEmpty()) {
                            endOfCrawling = true;
                        }
                        WebCrawler.alreadyCrawledDomain.clear();
                    }
                Thread.sleep(CRAWLER_SLEEP_IN_MILLISECONDS.get()/10);
                synchronized (domainsThatForbidCrawling) {
                    WebCrawler.domainsThatForbidCrawling.clear();
                    WebCrawler.domainsThatForbidCrawling.add("linkedin.com");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getFileNameInDir() {
        if (!filesFromDir.isEmpty()) {
            return filesFromDir.removeFirst();
        } else {
            return "";
        }
    }

    private void getHtmlFromLinksInFile(String fileName) {
            String query = FileHandler.getFileNameWithoutTxt(fileName);
            ArrayList<String> linesFromFile =
                FileHandler.getLinesFromAFile(DIRECTORY_WHERE_TO_GET_LINKS + File.separator + fileName);
            for (int positionOfTheLinkInFile = 0; positionOfTheLinkInFile < linesFromFile.size(); positionOfTheLinkInFile++) {
                String linkInFile = linesFromFile.get(positionOfTheLinkInFile);
                if (fileIsAlreadyDownloaded(query, positionOfTheLinkInFile, linkInFile)) {
                    continue;
                }
                getHtmlFromLink(linkInFile, query, positionOfTheLinkInFile);
            }
    }

    private boolean fileIsAlreadyDownloaded(String query, int positionOfTheLinkInFile,
        String linkInFile) {
        try {
            if (FileHandler
                .alreadyCreatedLinkElementsFileForQuerysLink(CRAWLED_LINKS_LINK_ELEMENTS_DIR
                    + File.separator
                    + query
                    + File.separator
                    + positionOfTheLinkInFile
                    + PageStatistics.TXT)) {
                return true;
            }
        } catch (IOException e) {
           handleException(linkInFile, e, "getHtmlFromLinksInFile IO");
        }
        return false;
    }

    private String getHtmlFromLink(String link, String query, int positionOfTheLinkInFile) {
        try {
            decideWhetherToSleep(link);
        } catch (IOException e) {
            handleException(link, e, "getHtmlFromLink IO");
        } catch (InterruptedException e) {
            handleException(link, e, "getHtmlFromLink InterruptedException");
        }
        return attemptToGetHtml(link, query, positionOfTheLinkInFile);
    }

    private String attemptToGetHtml(String link, String query, int positionOfTheLinkInFile) {
        String websitesHtml = "";
        try {
            synchronized (domainsThatForbidCrawling) {
                if (domainsThatForbidCrawling.contains(getUrlsTopPrivateDomainFromAbsoluteUrl(link))) {
                    return websitesHtml;
                }
            }

            URLConnection connection = getUrlConnection(link);
            if (!contentIsSmallEnough(connection)) {
                return websitesHtml;
            }

            Thread.sleep(CRAWLER_SLEEP_IN_MILLISECONDS.get()/10);

            websitesHtml = parseHtml(link, connection);

            saveResultsToFile(query, link, positionOfTheLinkInFile + 1, websitesHtml);
        } catch (IOException e) {
            if (!handleIoExceptionForConnection(link, e, "attemptToGetHtml IO")) {
                return "";
            }
        } catch (Exception exception) {
            handleException(link, exception, "attemptToGetHtml Exception");
        }
        return websitesHtml;
    }

    private void addCaughtExceptionToLog(String link, Exception exception, String message) {
        String loggedLine = "Exception "
               + exception.toString() + " ::: " + message + " @link " + link + "\n";
        FileHandler.saveTextLineToAFilePath(loggedLine, CAUGHT_EXCEPTIONS_FILE_PATH, true);
    }

    private void handleException(String link, Exception exception, String message) {
        System.out.println(link);
        System.out.println(message);
        addCaughtExceptionToLog(link,exception,message);
        exception.printStackTrace();
    }

    private boolean handleIoExceptionForConnection(String link, IOException e, String message) {
        handleException(link, e, message);
        if (e.getMessage().contains("403")
            || e.getMessage().contains("999")){
            synchronized (domainsThatForbidCrawling) {
                domainsThatForbidCrawling.add(getUrlsTopPrivateDomainFromAbsoluteUrl(link));
                System.out.println(link + " domain is done for, for now... " + e.getMessage());
            }
            return false;
        }
        return true;
    }

    private String parseHtml(String link, URLConnection connection)
        throws IOException {
        InputStream input = connection.getInputStream();
        String websitesHtml = Jsoup.parse(input, "UTF-8", link).body().html();
        input.close();
        return websitesHtml;
    }

    private URLConnection getUrlConnection(String link) throws IOException {
        URL url = new URL(link);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0");
        return connection;
    }

    private boolean contentIsSmallEnough(URLConnection connection) {
        int contentLength = connection.getContentLength();
        return contentLength < MAX_SIZE_OF_HTML_FILE.get()
            && contentLength >= 0;
    }

    private void saveResultsToFile(String query, String link, int positionOfTheLinkInFile, String websitesHtml)
        throws UnsupportedEncodingException {
        Elements elementsFromHtml = getLinksInsidePageFromLink(websitesHtml, link);
        if (!elementsFromHtml.isEmpty()) {
            FileHandler.saveIterableElementsToAFilePath(elementsFromHtml,
                CRAWLED_LINKS_LINK_ELEMENTS_DIR + File.separator + query + File.separator + positionOfTheLinkInFile + PageStatistics.TXT);

            String textFromHtml = getTextOnlyFromWebsitesHtml(websitesHtml, link);
            textFromHtml = textFromHtml.replaceAll(SEPARATOR, "\n");
            FileHandler.saveTextLineToAFilePath(textFromHtml
                , CRAWLED_LINKS_TEXT_DIR + File.separator + query + File.separator + positionOfTheLinkInFile + PageStatistics.TXT
                , APPEND_HTML_TO_TEXT_FILE);
        }
    }

    private Elements getLinksInsidePageFromLink(String websitesHtml, String link) {
        Elements foundLinkElements = new Elements();
        try {
            InputStream htmlAsIS = new ByteArrayInputStream(websitesHtml.getBytes(StandardCharsets.UTF_8));
            foundLinkElements = Jsoup.parse(htmlAsIS, "UTF-8", link).select("a[href]");
            htmlAsIS.close();
        } catch (IOException e) {
            handleException(link, e, "getLinksInsidePageFromLink Exception");
        }
        return foundLinkElements;
    }

    private String getTextOnlyFromWebsitesHtml(String websitesHtml, String link) {
        return Jsoup.parse(websitesHtml, link).body().text().replaceAll(AlphaNumericWhitespace, "");
    }

    private boolean decideWhetherToSleep(String link) throws IOException, InterruptedException {
        boolean sleep = false;
        synchronized (alreadyCrawledDomain) {
            synchronized (domainsThatForbidCrawling) {
                String topPrivateDomain = getUrlsTopPrivateDomainFromAbsoluteUrl(link);
                if (alreadyCrawledDomain.contains(topPrivateDomain)) {
                    sleep = true;
                } else if (!domainsThatForbidCrawling.contains(topPrivateDomain)) {
                    alreadyCrawledDomain.add(topPrivateDomain);
                }
            }
        }
        if(sleep) {
            Thread.sleep(CRAWLER_SLEEP_IN_MILLISECONDS.get() + ThreadLocalRandom
                .current().nextInt(0, CRAWLER_SLEEP_IN_MILLISECONDS.get()*4));
        }
        return sleep;
    }

    // Newest version of Guava should use topPrivateDomain().toString() method
    private String getUrlsTopPrivateDomainFromAbsoluteUrl(String absoluteUrl) {
        String urlsTopPrivateDomain;
        try {
            String host;
            host = new URL(absoluteUrl).getHost();
            urlsTopPrivateDomain = InternetDomainName.from(host).topPrivateDomain().name();
        } catch (IllegalStateException | MalformedURLException e) {
            e.printStackTrace();
            return "";
        }
        return urlsTopPrivateDomain;
    }
}
