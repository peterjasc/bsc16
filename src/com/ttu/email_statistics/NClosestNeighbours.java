package com.ttu.email_statistics;

import com.ttu.file_processing.FileHandler;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import static com.ttu.email_statistics.PageStatistics.*;

class NClosestNeighbours {

    private static final int HALF_OF_N_NUMBER_VALUE = 10;
    private static final int TWO_WORD_CASE_FIX = 1;

    private LinkedHashMap<String, String> nClosestWords = new LinkedHashMap<>();

    private static HashSet<String> stopWords = new HashSet<>();

    LinkedHashMap<String, String> getNClosestWords() {
        return nClosestWords;
    }

    void addNClosestNeighboursOfQueryInWebsitesText(String websitesText, String query, String linkInFile) {
        String[] splitHtml = websitesText.split(HTML_SEPARATOR);
        for (int wordsOrderInHtml = 0; wordsOrderInHtml < splitHtml.length; wordsOrderInHtml++) {
            if (NUMBER_OF_KEYWORDS == 2) {
                addNNeighboursForFoundTwoKeywordsInPage(linkInFile, splitHtml, wordsOrderInHtml,
                    query);
            } else if (NUMBER_OF_KEYWORDS == 1) {
                addNNeighboursForFoundKeywordInPage(linkInFile, splitHtml, wordsOrderInHtml,
                    query);
            }
        }
    }

    private void addNNeighboursForFoundKeywordInPage(String linkInFile, String[] splitHtml,
        int wordsOrderInHtml, String query) {
        String word = getNthWordInLowerCase(wordsOrderInHtml, splitHtml);
        if (StringUtils.containsIgnoreCase(query, word)
            || StringUtils.containsIgnoreCase(query, word)) {

            putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
            putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.RIGHT);
        }
    }

    private void addNNeighboursForFoundTwoKeywordsInPage(String linkInFile, String[] splitHtml,
        int wordsOrderInHtml, String query) {
        String word = getNthWordInLowerCase(wordsOrderInHtml, splitHtml);
        String[] emailAndFullName = query.split(QUERY_SEPARATOR, 2);
        String email = emailAndFullName[0];
        String fullName = emailAndFullName[1];

        boolean canSubtract = canSubtract(wordsOrderInHtml);
        boolean canAdd = canAdd(splitHtml, wordsOrderInHtml);


        canAddCase(linkInFile, splitHtml, wordsOrderInHtml, word, email, fullName, canAdd);
        canSubtractCase(linkInFile, splitHtml, wordsOrderInHtml, word, email, fullName,
            canSubtract);
    }

    private void canAddCase(String linkInFile, String[] splitHtml, int wordsOrderInHtml,
        String word, String email, String fullName, boolean canAdd) {
        if (canAdd) {
            // email fullname
            if (StringUtils.containsIgnoreCase(email,word) && StringUtils.containsIgnoreCase(
                fullName,
                getNthWordInLowerCase(wordsOrderInHtml + TWO_WORD_CASE_FIX, splitHtml))) {

                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml + TWO_WORD_CASE_FIX,
                    WhichSide.RIGHT);
            // email
            } else if (StringUtils.containsIgnoreCase(email, word) && !StringUtils
                .containsIgnoreCase(fullName,
                    getNthWordInLowerCase(wordsOrderInHtml + TWO_WORD_CASE_FIX, splitHtml))) {

                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.RIGHT);
            // fullname
            }
        } else {
            // email
            if (StringUtils.containsIgnoreCase(email, word)) {
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.RIGHT);
            }
        }
    }

    private void canSubtractCase(String linkInFile, String[] splitHtml, int wordsOrderInHtml,
        String word, String email, String fullName, boolean canSubtract) {
        if (canSubtract) {
            // email fullname
            if (!StringUtils.containsIgnoreCase(email,
                getNthWordInLowerCase(wordsOrderInHtml - TWO_WORD_CASE_FIX, splitHtml))
                && StringUtils.containsIgnoreCase(fullName, word)) {

                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.RIGHT);
            }
        } else {
            // fullname
            if (StringUtils.containsIgnoreCase(fullName, word)) {
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.LEFT);
                putNeighboursIntoList(linkInFile, splitHtml, wordsOrderInHtml, WhichSide.RIGHT);
            }
        }
    }

    private boolean canAdd(String[] splitHtml, int wordsOrderInHtml) {
        return wordsOrderInHtml + TWO_WORD_CASE_FIX < splitHtml.length;
    }

    private boolean canSubtract(int wordsOrderInHtml) {
        return wordsOrderInHtml - TWO_WORD_CASE_FIX >= 0;
    }

    private void putNeighboursIntoList(String linkInFile, String[] splitHtml,
        int wordsOrderInHtml, WhichSide whichSide) {
        for (int distanceFromWord = 1;
             distanceFromWord <= HALF_OF_N_NUMBER_VALUE; distanceFromWord++) {
            int whichWordInHtmlToGet = 0;
            if (whichSide == WhichSide.LEFT) {
                whichWordInHtmlToGet = wordsOrderInHtml - distanceFromWord;
            } else if (whichSide == WhichSide.RIGHT) {
                whichWordInHtmlToGet = wordsOrderInHtml + distanceFromWord;
            }
            if (whichWordInHtmlToGet < splitHtml.length && whichWordInHtmlToGet >= 0) {
                nClosestWords.put(linkInFile + SEPARATOR + whichSide.toString() + SEPARATOR + distanceFromWord,
                    getNthWordInLowerCase(whichWordInHtmlToGet, splitHtml));
            } else {
                break;
            }
        }
    }

    static void putStopWordsFromFileIntoCache() {
        ArrayList<String> stopWordFileLines =
            FileHandler.getLinesFromAFile(STOP_WORDS_FILE_PATH);
        stopWords.addAll(stopWordFileLines);
    }

}
