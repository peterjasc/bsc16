package com.ttu.email_statistics;

import com.ttu.file_processing.FileHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import static com.ttu.file_processing.FileHandler.fileWithFilePathDirQueryDirFileNumberExists;
import static com.ttu.file_processing.FileHandler.getFullFileNamesFromFilePathInAlphabeticalOrder;

public class PageStatistics {

    static final String SEPARATOR = " ";
    static final String HTML_SEPARATOR = " ";
    public static final String QUERY_SEPARATOR = " ";
    public static String TXT = ".txt";
    public static final String TEXT_ANALYSIS_DIR = "TextAnalysis";
    private static final String EXACT = "_exact";

    private static final String CRAWLER_DIR = "Crawler";
    private static final String N_CLOSEST_DIR_PATH = TEXT_ANALYSIS_DIR
        + File.separator + "NClosest";
    static final String STOP_WORDS_FILE_PATH = TEXT_ANALYSIS_DIR
        + File.separator + "stopWords.txt";
    private static final String CATALOGUE_COUNTER_DIR_PATH = TEXT_ANALYSIS_DIR
        + File.separator + "CatalogueCounter";

    // things that change often
    private static final String DIRECTORY_WHERE_TO_GET_EMAILS_AND_FULLNAMES
        = "emailsAndFullNamesForKWP";
    static final int NUMBER_OF_KEYWORDS = 1;
    static final WhichSubstring WHICH_SUBSTRING = WhichSubstring.FULL_NAME;
    private static final String PREFIX = "bing_fullNames";

    private static final String
        KEYWORD_PRESENCE_IN_TEXT_FILE_PATH = TEXT_ANALYSIS_DIR
        + File.separator + PREFIX + "KeywordPresenceInText" + EXACT + TXT;
    private static final String
        KEYWORD_PRESENCE_IN_LINKS_FILE_PATH = TEXT_ANALYSIS_DIR
        + File.separator + PREFIX + "KeywordPresenceInLinks" + EXACT + TXT;
    private static final String DIRECTORY_WHERE_TO_GET_LINKS = PREFIX + EXACT;
    private static final String DIRECTORY_WHERE_TO_GET_CRAWLED_LINKS_LINK_ELEMENTS
        = TEXT_ANALYSIS_DIR + File.separator + CRAWLER_DIR
        + File.separator + PREFIX + "CrawledLinksLinkElements" + EXACT;
    private static final String DIRECTORY_WHERE_TO_GET_CRAWLED_LINKS_TEXT
        = TEXT_ANALYSIS_DIR + File.separator + CRAWLER_DIR
        + File.separator + PREFIX + "CrawledLinksText" + EXACT;

    private KeywordPresenceCounter keywordPresenceCounter;
    private CatalogueCounter catalogueCounter;
    private NClosestNeighbours nClosestNeighbours;


    enum WhichSide {
        LEFT, RIGHT
    }

    public enum WhichSubstring {
        EMAIL, FULL_NAME
    }

    enum WhichGender {
        GIRL, BOY, BOTH
    }

    static String getNthWordInLowerCase(int nNumber, String[] words) {
        return words[nNumber].toLowerCase();
    }

    public static void main(String args[]) throws IOException, InterruptedException {
        initStaticCaches();

        LinkedList<String> filesFromDir =
            getFullFileNamesFromFilePathInAlphabeticalOrder
                (DIRECTORY_WHERE_TO_GET_EMAILS_AND_FULLNAMES);
        for (String fileName : filesFromDir) {
            String emailAndFullName = FileHandler.getFileNameWithoutTxt(fileName);
            System.out.println("FN:"+fileName);
            PageStatistics pageStatistics = new PageStatistics();
            pageStatistics.keywordPresenceCounter = new KeywordPresenceCounter();
            int numberOfLinks = pageStatistics.processLinksInFile(emailAndFullName);

            pageStatistics.handleResultsForFile(emailAndFullName, numberOfLinks);
        }
    }

    private static String getQueryFromEmailAndFullName(String emailAndFullName) {
        String query = emailAndFullName;
        if (NUMBER_OF_KEYWORDS == 1 && WHICH_SUBSTRING.equals(WhichSubstring.EMAIL)) {
            query = emailAndFullName.split(SEPARATOR,2)[0];
        } else if (NUMBER_OF_KEYWORDS == 1 && WHICH_SUBSTRING.equals(WhichSubstring.FULL_NAME)) {
            query = emailAndFullName.split(SEPARATOR,2)[1];
        }
        return query;
    }

    private static void initStaticCaches() {
//        CatalogueCounter.fillBothBabyNamesCaches();
//        NClosestNeighbours.putStopWordsFromFileIntoCache();
    }

    private int processLinksInFile(String emailAndFullName)
    throws IOException, InterruptedException {
        int numberOfLinks = 0;

        String query = getQueryFromEmailAndFullName(emailAndFullName);
        String filePathForQuery = DIRECTORY_WHERE_TO_GET_LINKS
                + File.separator + query + TXT;
        ArrayList<String> linesFromFile;

        if (FileHandler.filePathExists(filePathForQuery)) {
            linesFromFile = FileHandler
                .getLinesFromAFile(filePathForQuery);
        } else {
            return -1;
        }


        addCounterEntriesForFile(query);

        for (int positionOfLinkInFile = 0
             ; positionOfLinkInFile < linesFromFile.size()
            ; positionOfLinkInFile++) {
            String linkInFile = linesFromFile.get(positionOfLinkInFile);
//            System.out.println("LIF:" + linkInFile);

            numberOfLinks++;

            //            addCounterEntriesForLink(linkInFile);
            if(fileWithFilePathDirQueryDirFileNumberExists
                (DIRECTORY_WHERE_TO_GET_CRAWLED_LINKS_TEXT
                    , query, positionOfLinkInFile + 1)) {
                getStatisticsForPage(emailAndFullName, linkInFile, positionOfLinkInFile + 1);
            }

            //        handleResultsForLink(query, linkInFile, htmlFromPage);
        }
        return numberOfLinks;
    }

    private void addCounterEntriesForLink(String query) {
        catalogueCounter = new CatalogueCounter();
        catalogueCounter.addCatalogueCounterEntry(query);

        nClosestNeighbours = new NClosestNeighbours();
    }

    private void addCounterEntriesForFile(String query) {
        keywordPresenceCounter.addQueryPresenceEntry(query);
    }

    private void getStatisticsForPage(String emailAndFullName, String linkInFile
        , int positionOfLinkInFile)
        throws IOException, InterruptedException {
            String queryFolderNameInDir = getQueryFromEmailAndFullName(emailAndFullName);

            ArrayList<String> crawledTextFromFile =
                getTextFileFromQuerysFolderInDir(DIRECTORY_WHERE_TO_GET_CRAWLED_LINKS_TEXT,
                    queryFolderNameInDir, positionOfLinkInFile + TXT);

            ArrayList<String> crawledLinksFromFile =
                getTextFileFromQuerysFolderInDir(DIRECTORY_WHERE_TO_GET_CRAWLED_LINKS_LINK_ELEMENTS,
                    queryFolderNameInDir, positionOfLinkInFile + TXT);

            keywordPresenceCounter
                .countKeywordPresence(crawledTextFromFile
                    , crawledLinksFromFile, emailAndFullName);
            keywordPresenceCounter.countFilePresenceForQuery(queryFolderNameInDir);
    }
    private ArrayList<String> getTextFileFromQuerysFolderInDir(String directory, String queryFolderNameInDir,
        String fileInQuerysFolders) {
        return FileHandler.getLinesFromAFile(
            FileHandler.getFilePathDirQueryDirFileInQueryDir(directory, queryFolderNameInDir,
                fileInQuerysFolders));
    }

    private void handleResultsForFile(String emailAndFullName, int numberOfLinks) {
        handleKeywordPresenceResultsForQuery(emailAndFullName, numberOfLinks);
    }

    private void handleResultsForLink(String query, String linkInFile, String htmlFromPage) {
//        String websiteText = webCrawler.getTextOnlyFromWebsitesHtml(htmlFromPage, linkInFile);

//        catalogueCounter.addNamePresenceOccurrencesInWebsitesText(websiteText, linkInFile);

//        Elements websiteLinks = webCrawler.getLinksInsidePageFromLink(htmlFromPage,linkInFile);
//        catalogueCounter.addNamePresenceOccurrencesInWebsitesLinks(websiteLinks, linkInFile);

//        handleCatalogueCounterResultsForQuery(query);

//        nClosestNeighbours.addNClosestNeighboursOfQueryInWebsitesText(websiteText, query, linkInFile);
//        handleNClosestResultsForQuery(query);
    }

    private void handleNClosestResultsForQuery(String query) {
        FileHandler
            .saveHashMapStatsToAFilePath(N_CLOSEST_DIR_PATH + File.separator + query + ".txt",
               nClosestNeighbours.getNClosestWords());
    }

    private void handleCatalogueCounterResultsForQuery(String query) {
        FileHandler
            .saveStatsToAFilePath(CATALOGUE_COUNTER_DIR_PATH + File.separator + query + ".txt",
                catalogueCounter.getNameInLinkPresence(),
                catalogueCounter.getNamePresence());
    }

    private void handleKeywordPresenceResultsForQuery(String emailAndFullName, int numberOfLinksInFile) {
        if (numberOfLinksInFile != -1) {
            LinkedHashMap<String, Integer> keywordPresenceInText = keywordPresenceCounter.getKeywordPresenceInText();
            LinkedHashMap<String, Integer> keywordPresenceInLinkElements = keywordPresenceCounter.getKeywordPresenceInLinks();
            LinkedHashMap<String, Integer> filePresenceInQuery = keywordPresenceCounter.getFilePresenceForQuerys();

            FileHandler
                .saveStatsToAFilePath(KEYWORD_PRESENCE_IN_TEXT_FILE_PATH, keywordPresenceInText,
                    filePresenceInQuery);
            FileHandler.saveStatsToAFilePath(KEYWORD_PRESENCE_IN_LINKS_FILE_PATH,
                keywordPresenceInLinkElements, filePresenceInQuery);

        }
    }
}
