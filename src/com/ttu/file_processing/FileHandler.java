package com.ttu.file_processing;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.ttu.email_statistics.PageStatistics.TXT;

public class FileHandler {

    private static final AtomicBoolean APPEND = new AtomicBoolean(true);
    // for GCS, PageStatistics, DuplicateLRIFP need to change APPEND to TRUE!

    private static synchronized File getFile(String filePath) throws IOException {
        return new File(filePath);
    }
    private static synchronized File saveFile(String filePath) throws IOException {
        File file = new File(filePath);
        File parentFile = file.getParentFile();
        parentFile.mkdirs();
        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw  new IOException("Could not create file in the following filepath: "
                    + filePath);
            }
        }
        return file;
    }

    public static synchronized boolean alreadyCreatedLinkElementsFileForQuerysLink(String filePath) throws IOException {

        File file = new File(filePath);
        return file.exists() && file.length() > 0L;
    }

    public static synchronized LinkedList<String>
    getFullFileNamesFromFilePathInAlphabeticalOrder(String path) throws IOException {

        LinkedList<String> fileNamesInDir = new LinkedList<>();
        File[] files = new File(path).listFiles();
        if (files != null) {
            Arrays.sort(files);
            for (File file : files) {
                fileNamesInDir.add(file.getName());
            }
        }
        return fileNamesInDir;
    }

    public static synchronized ArrayList<String> getLinesFromAFile(String filePath) {
        BufferedReader br = null;
        ArrayList<String> lines = new ArrayList<>();
        try {
            File file = getFile(filePath);
            br = new BufferedReader(new FileReader(file));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lines;
    }

    public static synchronized void saveTextLineToAFilePath(String textLine, String filePath, boolean append) {
        BufferedWriter br = null;
        try {
            File file = saveFile(filePath);
            br = new BufferedWriter(new FileWriter(file, append));
                br.write(textLine);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized void saveIterableElementsToAFilePath(Iterable<?> iterable, String filePath) {
        BufferedWriter br = null;
        try {
            File file = saveFile(filePath);
            br = new BufferedWriter(new FileWriter(file, APPEND.get()));
            for (Object next : iterable) {
                br.write(next.toString() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveHashMapStatsToAFilePath(String filePath,
        HashMap<?, ?> hashMap) {
        BufferedWriter br = null;
        try {
            File file = saveFile(filePath);
            br = new BufferedWriter(new FileWriter(file, APPEND.get()));
            for (Map.Entry<?,?> next : hashMap.entrySet()) {
                br.write(next.getKey() + " " + next.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveStatsToAFilePath(String filePath
        , LinkedHashMap<String,Integer> linksContainingQuery
        , LinkedHashMap<String,Integer> filePresenceInQuery) {
        BufferedWriter br = null;
        try {
            File file = saveFile(filePath);
            br = new BufferedWriter(new FileWriter(file, APPEND.get()));
            for (Map.Entry<String,Integer> next : linksContainingQuery.entrySet()) {
                br.write(next.getKey() + " contains " + next.getValue()  + "/" + filePresenceInQuery.get(next.getKey()) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized String getFileNameWithoutTxt(String fileName) {
        return fileName.replace(".txt","");
    }

    public static boolean fileWithFilePathDirQueryDirFileNumberExists(String directory
        , String queryFolderNameInDir, int positionOfLinkInFile) {
        return new File
            (getFilePathDirQueryDirFileInQueryDir
                (directory, queryFolderNameInDir, positionOfLinkInFile + TXT)).exists();
    }

    public static String getFilePathDirQueryDirFileInQueryDir(String directory,
        String queryFolderNameInDir, String fileInQuerysFolders) {
        return directory
        + File.separator
        + queryFolderNameInDir
        + File.separator
        + fileInQuerysFolders;
    }

    public static boolean filePathExists(String filePath) {
        return new File(filePath).exists();
    }
}
