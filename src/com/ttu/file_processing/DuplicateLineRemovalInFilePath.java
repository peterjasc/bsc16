package com.ttu.file_processing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class DuplicateLineRemovalInFilePath {


    private static final String DIR_PATH = "i";

    public static void main(String args[]) throws IOException {
        LinkedList<String> filePathsInDir = FileHandler
            .getFullFileNamesFromFilePathInAlphabeticalOrder(DIR_PATH);

        for (String fileName : filePathsInDir) {
            System.out.println(fileName);
            ArrayList<String> linesFromFile =
                FileHandler.getLinesFromAFile(DIR_PATH + File.separator + fileName);
            LinkedList<String> toBeAddedLines = new LinkedList<>();
            for (String lineFromDir : linesFromFile) {
                boolean alreadyAdded = false;
                if (toBeAddedLines.contains(lineFromDir)) {
                    System.out.println(lineFromDir);
                    alreadyAdded = true;
                }

                if (!alreadyAdded) {
                    toBeAddedLines.add(lineFromDir);
                }
            }
            FileHandler.saveIterableElementsToAFilePath(toBeAddedLines,DIR_PATH + File.separator + fileName);
        }
    }
}
