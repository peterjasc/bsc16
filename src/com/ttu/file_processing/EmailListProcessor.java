package com.ttu.file_processing;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailListProcessor {

    public static final String EMAIL_REGEX =
        "[A-Za-z]{3,}[\\.\\-_][A-Za-z]{3,}@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}";

    public static void main(String args[]) throws IOException {
      removeUnneccesaryEmailsFromDir("bingEmails2");
//    LinkedList<String> lines = FileHandler.getLinesFromAFile("bs_EmailhunterEmails");
//    LinkedList<String> emails = getEmailsFromList(lines);
//    LinkedList<String> names = getNamesFromEmails(emails);
//    FileHandler.saveIterableElementsToAFilePath("bs_EmailhunterEmailsClean", emails);
//    FileHandler.saveIterableElementsToAFilePath("bs_EmailhunterFullNamesClean", names);
    }

    // only works, for joe.mcdonald@hi.com, not for joe99@hi.com
    static LinkedList<String> getNamesFromEmails(LinkedList<String> emails) {
        LinkedList<String> names = new LinkedList<>();
        for (String email : emails) {
            String[] splitEmail = email.split("[\\.\\-_@]");
            String fullName = splitEmail[0] + " " + splitEmail[1];
            names.add(fullName);
        }
        return names;
    }

    private static LinkedList<String> getEmailsFromList(LinkedList<String> strings) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        LinkedList<String> emails = new LinkedList<>();

        for (String string : strings) {
            Matcher matcher = pattern.matcher(string);
            while (matcher.find()) {
                String email = matcher.group();
                emails.add(email);
            }
        }
        return emails;
    }

    // for bing search results, where our regex was less strict
    private static LinkedList<String> removeUnneccesaryEmailsFromDir (String Directory)
        throws IOException {
        LinkedList<String> removedEmails = new LinkedList<>();
        LinkedList<String> filePathsInDir = FileHandler.getFullFileNamesFromFilePathInAlphabeticalOrder(Directory);
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        for (String fileName : filePathsInDir) {
            Matcher matcher = pattern.matcher(fileName);
            if (!matcher.find()) {
                if (new File(Directory + File.separator + fileName).delete()) {
                    removedEmails.add(fileName);
                } else {
                    System.out.println(fileName);
                }
            }
        }
        return removedEmails;
    }

}

