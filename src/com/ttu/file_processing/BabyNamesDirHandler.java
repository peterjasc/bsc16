package com.ttu.file_processing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static com.ttu.email_statistics.PageStatistics.TEXT_ANALYSIS_DIR;

public class BabyNamesDirHandler {

    public static final String BOY_NAMES_TXT = "BoyNames.txt";
    public static final String GIRL_NAMES_TXT = "GirlNames.txt";
    public static final String US_BABY_NAMES_DIR = "USBabyNames";
    public static final String MALE = "M";
    public static final String FEMALE = "F";
    static HashMap<String,Integer> boyBabyNames = new HashMap<>();
    static HashMap<String,Integer> girlBabyNames = new HashMap<>();

    public static void main(String args[]) throws IOException {
        LinkedList<String> filePathsInDir = FileHandler.getFullFileNamesFromFilePathInAlphabeticalOrder(US_BABY_NAMES_DIR);

        for (String fileName : filePathsInDir) {
            System.out.println(fileName);
            ArrayList<String> linesFromFile =
                FileHandler.getLinesFromAFile(US_BABY_NAMES_DIR + File.separator + fileName);
            for (String lineFromDir : linesFromFile) {
                String[] splitLineFromDir = lineFromDir.split(",", 3);
                String name = splitLineFromDir[0];
                String gender = splitLineFromDir[1];
                Integer numberOfOccurences = Integer.valueOf(splitLineFromDir[2]);
                if (gender.equals(FEMALE)) {
                    addNamesOccurrences(name, numberOfOccurences, girlBabyNames);
                } else if (gender.equals(MALE)){
                    addNamesOccurrences(name, numberOfOccurences, boyBabyNames);
                }
            }
        }
        FileHandler.saveHashMapStatsToAFilePath(TEXT_ANALYSIS_DIR + File.separator + GIRL_NAMES_TXT, girlBabyNames);
        FileHandler.saveHashMapStatsToAFilePath(TEXT_ANALYSIS_DIR + File.separator + BOY_NAMES_TXT, boyBabyNames);
    }

    static int addNamesOccurrences(String name, Integer numberOfOccurrences, HashMap<String,Integer> names) {
        if (names.containsKey(name)) {
            numberOfOccurrences = names.get(name) + numberOfOccurrences;
        }
        names.put(name, numberOfOccurrences);
        return numberOfOccurrences;
    }
}
