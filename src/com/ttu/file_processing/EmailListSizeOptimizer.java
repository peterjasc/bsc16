package com.ttu.file_processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;


public class EmailListSizeOptimizer {

    private static final String FROM = "bs_emails";
    private static final String INTO = "bs_emails_dup_rem_only_ten";
    private static final String INTO2 = "bs_fullnames_dup_rem_only_ten";
    private static final Locale CHAR_LOCALE = Locale.US;

    public static void main(String args[]) {

    ArrayList<String> linesInFile = FileHandler.getLinesFromAFile(FROM);
    saveNPercentPerDomain(linesInFile);
    
  }

    private static void saveNPercentPerDomain(ArrayList<String> linesInFile) {
        HashMap<String,Integer> emailsForDomain = new HashMap<>();
        LinkedList<String> toBeAddedEmails = new LinkedList<>();

        for (String toBeAddedLine : linesInFile) {
            String domainName = getDomainFromEmailString(toBeAddedLine);
            domainName = domainName.toLowerCase(CHAR_LOCALE);

            emailsForDomain.putIfAbsent(domainName, 0);
        }

        for (String toBeAddedLine : linesInFile) {
            toBeAddedLine = toBeAddedLine.toLowerCase(CHAR_LOCALE);

            String domainName = getDomainFromEmailString(toBeAddedLine);

            if (emailsForDomain.get(domainName) == null) {
                emailsForDomain.put(domainName, 1);
            } else if (Math.random() < 0.3) {
                Integer previousValue = emailsForDomain.get(domainName);
                emailsForDomain.put(domainName, ++previousValue);
            } else {
                continue;
            }

            toBeAddedEmails.add(toBeAddedLine);
        }

        if (toBeAddedEmails.size() != 0) {
            FileHandler.saveIterableElementsToAFilePath(toBeAddedEmails,INTO);
            LinkedList<String> fullNames =
                    EmailListProcessor.getNamesFromEmails(toBeAddedEmails);
            FileHandler.saveIterableElementsToAFilePath(fullNames, INTO2);
        }

    }

    private static String getDomainFromEmailString(String toBeAddedLine) {
        return toBeAddedLine.split("@")[1];
    }


}
