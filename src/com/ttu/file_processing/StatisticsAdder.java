package com.ttu.file_processing;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ttu.email_statistics.PageStatistics.*;
import static com.ttu.email_statistics.PageStatistics.TEXT_ANALYSIS_DIR;

public class StatisticsAdder {


    private static final String FILE_PATH = TEXT_ANALYSIS_DIR
        + File.separator + "bing_emailsKeywordPresenceInLinks_exact.txt";

    public static void main(String args[]) throws IOException {
            ArrayList<String> linesFromFile = FileHandler.getLinesFromAFile(FILE_PATH);
            Integer emailNumber = 0;
            Integer fullNameNumber = 0;
            Integer eAndFNumber = 0;
            Integer emailNumberVerify = 0;
            Integer fullNameNumberVerify = 0;
            Integer eAndFNumberVerify = 0;
            for (String line : linesFromFile) {
                String[] howManyOutOfHowMany = new String[]{};
                boolean containsEmailAsText =
                    StringUtils.contains(line, WhichSubstring.EMAIL.toString());
                boolean containsFullNameAsText =
                    StringUtils.contains(line, WhichSubstring.FULL_NAME.toString());

                Pattern p = Pattern.compile("[\\d]{1,2}\\/[\\d]{1,2}");
                Matcher matcher = p.matcher(line);
                boolean matchFound = matcher.find();
                if (matchFound) {
                    howManyOutOfHowMany = matcher.group().split("/",2);
                }

                if (containsEmailAsText && matchFound) {
                    emailNumber = getHowMany(emailNumber, howManyOutOfHowMany);
                    emailNumberVerify = getOutOfHowMany(emailNumberVerify, howManyOutOfHowMany);
                } else if (containsFullNameAsText && matchFound) {
                    fullNameNumber = getHowMany(fullNameNumber, howManyOutOfHowMany);
                    fullNameNumberVerify = getOutOfHowMany(fullNameNumberVerify, howManyOutOfHowMany);
                } else if (!line.isEmpty() && matchFound) {
                    eAndFNumber = getHowMany(eAndFNumber, howManyOutOfHowMany);
                    eAndFNumberVerify = getOutOfHowMany(eAndFNumberVerify, howManyOutOfHowMany);
                }

            }
            System.out.println("eN");
        System.out.println(emailNumber);
            System.out.println(emailNumberVerify);
            System.out.println("fN");
        System.out.println(fullNameNumber);
            System.out.println(fullNameNumberVerify);
            System.out.println("eAFN");
        System.out.println(eAndFNumber);
            System.out.println(eAndFNumberVerify);

    }

    private static int getOutOfHowMany(Integer emailNumberVerify, String[] howManyOutOfHowMany) {
        return emailNumberVerify + Integer.valueOf(howManyOutOfHowMany[1]);
    }

    private static int getHowMany(Integer emailNumber, String[] howManyOutOfHowMany) {
        return emailNumber + Integer.valueOf(howManyOutOfHowMany[0]);
    }
}
